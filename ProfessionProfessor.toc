## Interface: 11304
## Title: Profession Professor
## Author: Gyldendahl-MirageRaceway-EU
## Notes: Exports profession skills to be used with discord
## Version: 1.13.4
## SavedVariables: ProfessionProfessorDB

Libs\LibStub\LibStub.lua
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceSerializer-3.0\AceSerializer-3.0.xml
Libs\AceEvent-3.0\AceEvent-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml
Libs\AceDB-3.0\AceDB-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml

Skills.lua
ProfessionProfessor.lua
